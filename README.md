# easycec

A simple CEC daemon for HDMI-CEC

### ADDING A CUSTOM KEYMAP & ENABLING THE KEYMAP

- Create a section in keymap.ini example: [yourdevicenamehere-keymap]
- Open cec-daemon.py and modify "keyconfig = config._sections['default-keymap']" to keyconfig = config._sections['yourdevicenamehere-keymap']
